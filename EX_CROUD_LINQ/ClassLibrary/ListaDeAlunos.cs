﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class ListaDeAlunos
    {
        public static List<Aluno> LoadAlunos()
        {
            List<Aluno> output = new List<Aluno>();

            output.Add(new Aluno { PrimeiroNome = "Amadeu", Apelido = "Antunes", IdAluno = 100});
            output.Add(new Aluno { PrimeiroNome = "Antonio", Apelido = "Almeida", IdAluno = 101});
            output.Add(new Aluno { PrimeiroNome = "Daniel", Apelido = "Veiga", IdAluno = 102});
            output.Add(new Aluno { PrimeiroNome = "Joao", Apelido = "Ribeiro", IdAluno = 103});
            output.Add(new Aluno { PrimeiroNome = "Luis", Apelido = "Gomes", IdAluno = 104});
            output.Add(new Aluno { PrimeiroNome = "Hugo", Apelido = "Pereira", IdAluno = 105});
            output.Add(new Aluno { PrimeiroNome = "Nuno", Apelido = "Serano", IdAluno = 106});
            
            return output;
        }
    }
}
