﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Aluno
    {
        public int IdAluno { get; set; }

        public string PrimeiroNome { get; set; }

        public string Apelido { get; set; }

        public string NomeCompleto
        {
            get
            {
                return string.Format("ID: {0}  Nome: {1} {2}",IdAluno, PrimeiroNome, Apelido);
            }

        }

        public string NomeApelido => $"{PrimeiroNome} {Apelido}";
    }
}
