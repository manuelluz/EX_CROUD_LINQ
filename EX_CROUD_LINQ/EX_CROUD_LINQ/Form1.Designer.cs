﻿namespace EX_CROUD_LINQ
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ComboBoxAlunos = new System.Windows.Forms.ComboBox();
            this.ComboBoxListaAlunos = new System.Windows.Forms.ComboBox();
            this.ComboBoxProcurar = new System.Windows.Forms.ComboBox();
            this.TextBoxPorcurar = new System.Windows.Forms.TextBox();
            this.TextBoxIdAluno = new System.Windows.Forms.TextBox();
            this.TextBoxNomeAluno = new System.Windows.Forms.TextBox();
            this.TextBoxPrimeiroNome = new System.Windows.Forms.TextBox();
            this.TextBoxApelido = new System.Windows.Forms.TextBox();
            this.ButtonEditar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ButtonCancelar = new System.Windows.Forms.Button();
            this.ButtonGravar = new System.Windows.Forms.Button();
            this.ButtonProcurar = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.alunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atualizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ComboBoxListaAlunos);
            this.groupBox1.Controls.Add(this.ComboBoxAlunos);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(335, 110);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Listagem";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ButtonProcurar);
            this.groupBox2.Controls.Add(this.TextBoxPorcurar);
            this.groupBox2.Controls.Add(this.ComboBoxProcurar);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(353, 23);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(248, 110);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Procurar";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.ButtonGravar);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.ButtonCancelar);
            this.groupBox3.Controls.Add(this.ButtonEditar);
            this.groupBox3.Controls.Add(this.TextBoxApelido);
            this.groupBox3.Controls.Add(this.TextBoxPrimeiroNome);
            this.groupBox3.Controls.Add(this.TextBoxNomeAluno);
            this.groupBox3.Controls.Add(this.TextBoxIdAluno);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(12, 139);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(589, 110);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Editar";
            // 
            // ComboBoxAlunos
            // 
            this.ComboBoxAlunos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxAlunos.FormattingEnabled = true;
            this.ComboBoxAlunos.Location = new System.Drawing.Point(7, 32);
            this.ComboBoxAlunos.Name = "ComboBoxAlunos";
            this.ComboBoxAlunos.Size = new System.Drawing.Size(300, 24);
            this.ComboBoxAlunos.TabIndex = 0;
            this.ComboBoxAlunos.SelectedIndexChanged += new System.EventHandler(this.ComboBoxAlunos_SelectedIndexChanged);
            // 
            // ComboBoxListaAlunos
            // 
            this.ComboBoxListaAlunos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxListaAlunos.FormattingEnabled = true;
            this.ComboBoxListaAlunos.Location = new System.Drawing.Point(7, 68);
            this.ComboBoxListaAlunos.Name = "ComboBoxListaAlunos";
            this.ComboBoxListaAlunos.Size = new System.Drawing.Size(121, 24);
            this.ComboBoxListaAlunos.TabIndex = 1;
            // 
            // ComboBoxProcurar
            // 
            this.ComboBoxProcurar.FormattingEnabled = true;
            this.ComboBoxProcurar.Location = new System.Drawing.Point(6, 32);
            this.ComboBoxProcurar.Name = "ComboBoxProcurar";
            this.ComboBoxProcurar.Size = new System.Drawing.Size(121, 24);
            this.ComboBoxProcurar.TabIndex = 2;
            // 
            // TextBoxPorcurar
            // 
            this.TextBoxPorcurar.Location = new System.Drawing.Point(133, 32);
            this.TextBoxPorcurar.Name = "TextBoxPorcurar";
            this.TextBoxPorcurar.Size = new System.Drawing.Size(100, 22);
            this.TextBoxPorcurar.TabIndex = 3;
            // 
            // TextBoxIdAluno
            // 
            this.TextBoxIdAluno.Location = new System.Drawing.Point(90, 29);
            this.TextBoxIdAluno.Name = "TextBoxIdAluno";
            this.TextBoxIdAluno.ReadOnly = true;
            this.TextBoxIdAluno.Size = new System.Drawing.Size(119, 22);
            this.TextBoxIdAluno.TabIndex = 4;
            // 
            // TextBoxNomeAluno
            // 
            this.TextBoxNomeAluno.Location = new System.Drawing.Point(347, 29);
            this.TextBoxNomeAluno.Name = "TextBoxNomeAluno";
            this.TextBoxNomeAluno.ReadOnly = true;
            this.TextBoxNomeAluno.Size = new System.Drawing.Size(131, 22);
            this.TextBoxNomeAluno.TabIndex = 5;
            // 
            // TextBoxPrimeiroNome
            // 
            this.TextBoxPrimeiroNome.Location = new System.Drawing.Point(90, 62);
            this.TextBoxPrimeiroNome.Name = "TextBoxPrimeiroNome";
            this.TextBoxPrimeiroNome.Size = new System.Drawing.Size(119, 22);
            this.TextBoxPrimeiroNome.TabIndex = 6;
            // 
            // TextBoxApelido
            // 
            this.TextBoxApelido.Location = new System.Drawing.Point(347, 62);
            this.TextBoxApelido.Name = "TextBoxApelido";
            this.TextBoxApelido.Size = new System.Drawing.Size(131, 22);
            this.TextBoxApelido.TabIndex = 7;
            // 
            // ButtonEditar
            // 
            this.ButtonEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonEditar.Location = new System.Drawing.Point(498, 24);
            this.ButtonEditar.Name = "ButtonEditar";
            this.ButtonEditar.Size = new System.Drawing.Size(76, 32);
            this.ButtonEditar.TabIndex = 5;
            this.ButtonEditar.Text = "Editar";
            this.ButtonEditar.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "ID Aluno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(249, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 16);
            this.label2.TabIndex = 11;
            this.label2.Text = "Nome Aluno";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 16);
            this.label3.TabIndex = 12;
            this.label3.Text = "Nome";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(279, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 16);
            this.label4.TabIndex = 13;
            this.label4.Text = "Apelido";
            // 
            // ButtonCancelar
            // 
            this.ButtonCancelar.BackgroundImage = global::EX_CROUD_LINQ.Properties.Resources.if_Close_Icon_1398919;
            this.ButtonCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonCancelar.Location = new System.Drawing.Point(539, 62);
            this.ButtonCancelar.Name = "ButtonCancelar";
            this.ButtonCancelar.Size = new System.Drawing.Size(35, 39);
            this.ButtonCancelar.TabIndex = 9;
            this.ButtonCancelar.UseVisualStyleBackColor = true;
            // 
            // ButtonGravar
            // 
            this.ButtonGravar.BackgroundImage = global::EX_CROUD_LINQ.Properties.Resources.if_Tick_Mark_1398911;
            this.ButtonGravar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonGravar.Location = new System.Drawing.Point(498, 63);
            this.ButtonGravar.Name = "ButtonGravar";
            this.ButtonGravar.Size = new System.Drawing.Size(35, 38);
            this.ButtonGravar.TabIndex = 8;
            this.ButtonGravar.UseVisualStyleBackColor = true;
            // 
            // ButtonProcurar
            // 
            this.ButtonProcurar.BackgroundImage = global::EX_CROUD_LINQ.Properties.Resources.if_67_111124;
            this.ButtonProcurar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonProcurar.Location = new System.Drawing.Point(200, 73);
            this.ButtonProcurar.Name = "ButtonProcurar";
            this.ButtonProcurar.Size = new System.Drawing.Size(33, 31);
            this.ButtonProcurar.TabIndex = 4;
            this.ButtonProcurar.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alunoToolStripMenuItem,
            this.atualizarToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(616, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // alunoToolStripMenuItem
            // 
            this.alunoToolStripMenuItem.Name = "alunoToolStripMenuItem";
            this.alunoToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.alunoToolStripMenuItem.Text = "Aluno";
            // 
            // atualizarToolStripMenuItem
            // 
            this.atualizarToolStripMenuItem.Name = "atualizarToolStripMenuItem";
            this.atualizarToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.atualizarToolStripMenuItem.Text = "Atualizar";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 260);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ComboBoxListaAlunos;
        private System.Windows.Forms.ComboBox ComboBoxAlunos;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button ButtonProcurar;
        private System.Windows.Forms.TextBox TextBoxPorcurar;
        private System.Windows.Forms.ComboBox ComboBoxProcurar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonCancelar;
        private System.Windows.Forms.Button ButtonGravar;
        private System.Windows.Forms.Button ButtonEditar;
        private System.Windows.Forms.TextBox TextBoxApelido;
        private System.Windows.Forms.TextBox TextBoxPrimeiroNome;
        private System.Windows.Forms.TextBox TextBoxNomeAluno;
        private System.Windows.Forms.TextBox TextBoxIdAluno;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem alunoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atualizarToolStripMenuItem;
    }
}

