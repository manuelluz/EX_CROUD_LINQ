﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX_CROUD_LINQ
{
    public partial class Form1 : Form
    {

        List<Aluno> Alunos = ListaDeAlunos.LoadAlunos();

        

        public Form1()
        {
            InitializeComponent();
            InitComob();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void InitComob()
        {
            ComboBoxAlunos.DataSource = Alunos;
            ComboBoxAlunos.DisplayMember = "NomeCompleto";

            ComboBoxListaAlunos.DataSource = Alunos;
            ComboBoxListaAlunos.DisplayMember ="NomeApelido";

        }

        private void ComboBoxAlunos_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}
